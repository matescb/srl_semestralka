%% Create system data with slTuner interface
TunedBlocks = {'NonLinearModel_orig/Elev'};
AnalysisPoints = {'NonLinearModel_orig/Constant/1'; ...
                  'NonLinearModel_orig/Switch/1'};
% Specify the custom options
Options = slTunerOptions('AreParamsTunable',false);
% Create the slTuner object
CL0 = slTuner('NonLinearModel_orig',TunedBlocks,AnalysisPoints,Options);

% Set the parameterization of the tuned block
NonLinearModel_orig_Elev = tunablePID('NonLinearModel_orig_Elev','pid');
NonLinearModel_orig_Elev.Kp.Value = -0.00707651864798253;
NonLinearModel_orig_Elev.Ki.Value = -0.000524234000350037;
NonLinearModel_orig_Elev.Kd.Value = 0;
NonLinearModel_orig_Elev.Tf.Value = 1;
setBlockParam(CL0,'NonLinearModel_orig/Elev',NonLinearModel_orig_Elev);

%% Create tuning goal to shape how the closed-loop system responds to a specific input signal
% Inputs and outputs
Inputs = {'NonLinearModel_orig/Constant/1[aoa]'};
Outputs = {'NonLinearModel_orig/Switch/1'};
% Tuning goal specifications
Tau = 1; % Time constant
Overshoot = 1; % Overshoot (%)
% Create tuning goal for step tracking
StepTrackingGoal1 = TuningGoal.StepTracking(Inputs,Outputs,Tau,Overshoot);
StepTrackingGoal1.Name = 'StepTrackingGoal1'; % Tuning goal name

%% Create option set for systune command
Options = systuneOptions();
Options.Display = 'off'; % Tuning display level ('final', 'sub', 'iter', 'off')

%% Set soft and hard goals
SoftGoals = [ StepTrackingGoal1 ];
HardGoals = [];

%% Tune the parameters with soft and hard goals
[CL1,fSoft,gHard,Info] = systune(CL0,SoftGoals,HardGoals,Options);

%% View tuning results
% viewSpec([SoftGoals;HardGoals],CL1);
